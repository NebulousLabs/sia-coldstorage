[![logo](./sia-coldstorage_175.svg)](http://sia.tech)

# sia-coldstorage

sia-coldstorage generates a seed and a collection of addresses for a Sia wallet 
without downloading the blockchain. This lets you create ‘cold storage’ for Sia - 
the coins you’ll send to these addresses are highly secure, even though they 
aren’t readily spendable.

## Security

While sia-coldstorage can be run on any computer, we recommend running this 
utility on an airgapped machine, which is a computer that is not currently (and 
preferably never has been) connected to the internet. This gives you a high 
degree of certainty that there’s no malware on the machine that might steal your
coins. For ever greater security, run it on an airgapped computer with a 
[LiveOS](https://en.wikipedia.org/wiki/Live_CD), which provides a high-security 
environment launched directly from external media.

Finally, don’t print out your seed and addresses. A printer could be compromised
with malicious software. Write it down instead.

## Usage

Download the latest
[release](https://gitlab.com/NebulousLabs/sia-coldstorage/-/releases) for your
platform.

Double click the `sia-coldstorage` binary. This will generate
your seed and addresses, opening up a page in your web browser to display them.

Securely store the seed and addresses. We recommend a digital copy in a secure
password-protected app, and a physical copy laminated and locked in a safe.
Everyone’s got their own risk tolerance, so just make sure you store them somewhere
safe that a malicious party wouldn’t be able to access.

Send Siacoins to any of the addresses to securely keep them in your new cold
storage wallet.

To access and spend the Siacoins in your cold wallet, restore your Sia seed. You
can do this in [Sia-UI](http://support.sia.tech/article/97ushgu2zd-sia-ui-restore-from-seed)
or in `siac` with the following commands:
- `siac wallet init-seed` will initialize a new wallet with your cold storage
seed
- `siac wallet sweep` will bring the funds from your cold wallet into your
existing wallet

## Links

[Sia's website](https://sia.tech)  
[Sia's support center](https://support.sia.tech)

## License

The MIT License (MIT)

## Credits

This project was forked from [Ava Howell's](https://github.com/avahowell) 
sia-coldstorage. Thanks for your hard work!